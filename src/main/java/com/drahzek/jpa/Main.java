package com.drahzek.jpa;

/**
 * Author : Dominik Swierzynski
 */
public class Main {
    public static void main(String[] args) {

        Service service = new Service();
        service.fillOutDatabase();

        //9
        System.out.println("All caves");
        service.printAllCaves();
        System.out.println("All goblins");
        service.printAllGoblins();
        System.out.println("All weapons");
        service.printAllWeapons();

        //10
//        System.out.println("Goblins without weapons");
//        service.findDefenselessGoblins();
        System.out.println("Weapons without owners");
        service.findLonelyWeapons();
        System.out.println("Empty Caves");
        service.findEmptyCaves();
        int value = 140;
        System.out.println("Goblin heigher than " + value);
        service.findGoblinsHeigherThanGivenValue(value);

        //11 - hehe, crash it
        service.deleteAllCaves();
        //service.deleteAllGoblins();
        //service.deleteAllWeapons();

        System.out.println("CRASH TEST");

        service.printAllCaves();
        service.printAllGoblins();
        service.printAllWeapons();

        GoblinsEntityManagerFactory.close();
    }
}

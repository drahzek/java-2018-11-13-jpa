package com.drahzek.jpa.dao;

import com.drahzek.jpa.GoblinsEntityManagerFactory;
import com.drahzek.jpa.model.Goblin;
import com.drahzek.jpa.model.Weapon;
import com.drahzek.jpa.model.Weapon_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class WeaponsDao {
//    public Weapon persist(Weapon weapon) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        em.persist(weapon);
//        tx.commit();
//        em.close();
//        return weapon;
//    }

    public void persist(Weapon weapon) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(weapon);
        em.getTransaction().commit();
        em.close();
    }

//    public Weapon merge(Weapon weapon) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        weapon = em.merge(weapon);
//        tx.commit();
//        em.close();
//        return weapon;
//    }

    public Weapon merge(Weapon weapon) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        weapon = em.merge(weapon);
        em.getTransaction().commit();
        em.close();
        return weapon;
    }

    public Weapon find(int id) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        Weapon weapon = em.find(Weapon.class, id);
        em.close();
        return weapon;
    }

//    public void delete(Weapon weapon) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        em.remove(em.merge(weapon));
//        tx.commit();
//        em.close();
//    }

    public void delete(Weapon weapon) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        weapon = em.merge(weapon);
        if (weapon.getGoblin() != null){
            em.merge(weapon.getGoblin()).setWeapon(null);
        }
        em.remove(weapon);

        em.getTransaction().commit();
        em.close();
    }

    public List<Weapon> findAll() {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Weapon> query = cb.createQuery(Weapon.class);
        query.from(Weapon.class);
        List<Weapon> weapons = em.createQuery(query).getResultList();
        em.close();
        return weapons;
    }

    public List<Weapon> findLonelyWeapons() {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Weapon> query = cb.createQuery(Weapon.class);
        Root<Weapon> root = query.from(Weapon.class);
        query.where(cb.isNull(root.get(Weapon_.goblin)));
        List <Weapon> weapons = em.createQuery(query).getResultList();
        em.close();
        return weapons;
    }
}

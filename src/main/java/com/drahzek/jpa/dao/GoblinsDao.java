package com.drahzek.jpa.dao;

import com.drahzek.jpa.GoblinsEntityManagerFactory;
import com.drahzek.jpa.model.Goblin;
import com.drahzek.jpa.model.Goblin_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class GoblinsDao {

//    public Goblin persist(Goblin goblin) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        em.persist(goblin);
//        tx.commit();
//        em.close();
//        return goblin;
//    }

    public void persist(Goblin goblin) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(goblin);
        em.getTransaction().commit();
        em.close();
    }

//    public Goblin merge(Goblin goblin) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        goblin = em.merge(goblin);
//        tx.commit();
//        em.close();
//        return goblin;
//    }

    public Goblin merge(Goblin goblin) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        goblin = em.merge(goblin);
        em.getTransaction().commit();
        em.close();
        return goblin;
    }

    public Goblin find(int id) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        Goblin goblin = em.find(Goblin.class, id);
        em.close();
        return goblin;
    }

//    public void delete(Goblin goblin) {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        em.remove(em.merge(goblin));
//        tx.commit();
//        em.close();
//    }

    public void delete(Goblin goblin) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        goblin = em.merge(goblin);
        if (goblin.getCave() != null) {
            em.merge(goblin.getCave()).getGoblins().remove(goblin);
        }
        em.remove(goblin);
        em.getTransaction().commit();
        em.close();
    }

    public List<Goblin> findAll() {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        query.from(Goblin.class);
        List<Goblin> goblins = em.createQuery(query).getResultList();
        em.close();
        return goblins;
    }

    //has an unknown problem with Goblin entity, even though this class is set to be one
//    public List<Goblin> findDefenselessGoblins() {
//        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
//        List list = em.createQuery("select g from Goblin g left join g.weap w on w.owner = g where w is null").getResultList();
//        em.close();
//        return list;
//    }

    public List<Goblin> findGoblinsHeigherThanGivenValue(int value) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        Root<Goblin> root = query.from(Goblin.class);
        query.where(cb.greaterThan(root.get(Goblin_.height), value));
        List <Goblin> goblins = em.createQuery(query).getResultList();
        em.close();
        return goblins;
    }
}

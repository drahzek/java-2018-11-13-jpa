package com.drahzek.jpa.dao;

import com.drahzek.jpa.GoblinsEntityManagerFactory;
import com.drahzek.jpa.model.Cave;
import com.drahzek.jpa.model.Cave_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CavesDao {
    public Cave persist(Cave cave) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(cave);
        tx.commit();
        em.close();
        return cave;
    }

    public Cave merge(Cave cave) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        cave = em.merge(cave);
        tx.commit();
        em.close();
        return cave;
    }

    public Cave find(int id) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        Cave cave = em.find(Cave.class, id);
        em.close();
        return cave;
    }

    public void delete(Cave cave) {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(cave));
        tx.commit();
        em.close();
    }

    public List<Cave> findAll() {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);
        query.from(Cave.class);
        List<Cave> caves = em.createQuery(query).getResultList();
        em.close();
        return caves;
    }

    public List<Cave> findEmptyCaves() {
        EntityManager em = GoblinsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);
        Root<Cave> root = query.from(Cave.class);
        query.where(cb.isEmpty(root.get(Cave_.goblins)));
        List <Cave> caves = em.createQuery(query).getResultList();
        em.close();
        return caves;
    }
}

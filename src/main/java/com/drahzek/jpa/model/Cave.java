package com.drahzek.jpa.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "caves")
public class Cave {

    /**
     * Primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * cave area.
     */
    @Column
    private double area;

    @OneToMany(mappedBy = "cave", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Goblin> goblins = new ArrayList<>();

    public Cave() {
    }

    public Cave(double area) {
        this.area = area;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public List<Goblin> getGoblins() {
        return goblins;
    }

    public void setGoblins(List<Goblin> goblins) {
        this.goblins = goblins;
    }

    @Override
    public String toString() {
        return "Cave{" +
                "id=" + id +
                ", area=" + area +
                ", goblins=" + goblins +
                '}';
    }
}

package com.drahzek.jpa.model;

import javax.persistence.*;

@Entity
@Table(name = "goblins")
public class Goblin {

    /**
     * Primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * goblin name.
     */
    @Column
    private String name;

    /**
     * goblin height.
     */
    @Column
    private int height;

    @OneToOne(mappedBy = "goblin", cascade = CascadeType.ALL, orphanRemoval = true)
    private Weapon weapon;

    @ManyToOne
    @JoinColumn(name = "cave", referencedColumnName = "id")
    private Cave cave;

    public Goblin() {
    }

    public Goblin(String name, int height, Cave cave) {
        this.name = name;
        this.height = height;
        this.cave = cave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Cave getCave() {
        return cave;
    }

    public void setCave(Cave cave) {
        this.cave = cave;
    }

    @Override
    public String toString() {
        return "Goblin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", weapon=" + (weapon != null ? weapon.getName() : null) +
                ", cave=" + (cave != null ? cave.getArea() : null) +
                '}';
    }
}

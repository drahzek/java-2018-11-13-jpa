package com.drahzek.jpa.model;

import javax.persistence.*;

@Entity
@Table(name = "weapons")
public class Weapon {

    /**
     * Primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * weapon name.
     */
    @Column
    private String name;

    /**
     * weapon damage.
     */
    @Column
    private int damage;

    /**
     * All available types of damage.
     */
    public enum DamageType {
        PIERCING,
        SLASHING,
        BLUNT;
    }

    @Enumerated(EnumType.STRING)
    @Column
    private DamageType damageType;

    @OneToOne(cascade = CascadeType.ALL)
    private Goblin goblin;

    public Weapon() {
    }

    public Weapon(String name, int damage, DamageType damageType) {
        this.name = name;
        this.damage = damage;
        this.damageType = damageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public void setDamageType(DamageType damageType) {
        this.damageType = damageType;
    }

    public Goblin getGoblin() {
        return goblin;
    }

    public void setGoblin(Goblin goblin) {
        this.goblin = goblin;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", damage=" + damage +
                ", damageType=" + damageType +
                ", goblin=" + (goblin != null ? goblin.getName() : null) +
                '}';
    }
}

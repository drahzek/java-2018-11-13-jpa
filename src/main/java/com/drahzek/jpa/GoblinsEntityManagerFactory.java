package com.drahzek.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GoblinsEntityManagerFactory {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("GoblinsPU");

    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }
}

package com.drahzek.jpa;

import com.drahzek.jpa.dao.CavesDao;
import com.drahzek.jpa.dao.GoblinsDao;
import com.drahzek.jpa.dao.WeaponsDao;
import com.drahzek.jpa.model.Cave;
import com.drahzek.jpa.model.Goblin;
import com.drahzek.jpa.model.Weapon;

public class Service {
    CavesDao cavesDao = new CavesDao();
    GoblinsDao goblinsDao = new GoblinsDao();
    WeaponsDao weaponsDao = new WeaponsDao();

    public void fillOutDatabase () {

        Cave cave1 = new Cave(300);
        Cave cave2 = new Cave(450);
        Cave cave3 = new Cave(350);

        Goblin edwin = new Goblin("Edwin", 150, cave1);
        Goblin gregor = new Goblin("Gregor", 146, cave1);
        Goblin jaimie = new Goblin("Jaimie", 134, cave1);
        Goblin jo = new Goblin("Jo", 122, cave1);
        Goblin grishnak = new Goblin("Grishnak", 145, cave2);
        Goblin gork = new Goblin("Gork", 160, cave2);
        Goblin mork = new Goblin("Mork", 165, cave2);
        Goblin skarsnik = new Goblin("Skarsnik", 139, cave2);

        Weapon knife1 = new Weapon("Rusted Knife", 11, Weapon.DamageType.SLASHING);
        Weapon knife2 = new Weapon("Dirty Knife", 12, Weapon.DamageType.SLASHING);
        Weapon knife3 = new Weapon("Old Knife", 14, Weapon.DamageType.SLASHING);
        Weapon knife4 = new Weapon("Weathered Knife", 22, Weapon.DamageType.SLASHING);
        Weapon club1 = new Weapon("Rusted Club", 44, Weapon.DamageType.BLUNT);
        Weapon club2 = new Weapon("Wooden Club", 31, Weapon.DamageType.BLUNT);
        Weapon needle1 = new Weapon("Rusted Needle", 22, Weapon.DamageType.PIERCING);
        Weapon needle2 = new Weapon("Old Needle", 33, Weapon.DamageType.PIERCING);

        knife1.setGoblin(edwin);
        edwin.setWeapon(knife1);

        knife2.setGoblin(gregor);
        gregor.setWeapon(knife2);

        knife3.setGoblin(jaimie);
        jaimie.setWeapon(knife3);

        club1.setGoblin(gork);
        gork.setWeapon(club1);

        club2.setGoblin(mork);
        mork.setWeapon(club2);

        needle1.setGoblin(skarsnik);
        skarsnik.setWeapon(needle1);

        cave1.getGoblins().add(edwin);
        cave1.getGoblins().add(gregor);
        cave1.getGoblins().add(jaimie);
        cave2.getGoblins().add(jo);
        cave2.getGoblins().add(grishnak);
        cave2.getGoblins().add(gork);
        cave2.getGoblins().add(mork);
        cave2.getGoblins().add(skarsnik);

        cavesDao.persist(cave1);
        cavesDao.persist(cave2);
        cavesDao.persist(cave3);
        weaponsDao.persist(knife4);
        weaponsDao.persist(needle2);
    }

    public void printAllCaves () {
        cavesDao.findAll().forEach(System.out::println);
    }

    public void printAllGoblins () {
        goblinsDao.findAll().forEach(System.out::println);
    }

    public void printAllWeapons () {
        weaponsDao.findAll().forEach(System.out::println);
    }

    public void deleteAllCaves () {
        cavesDao.findAll().forEach(e -> cavesDao.delete(e));
    }

    public void deleteAllGoblins () {
        goblinsDao.findAll().forEach(e-> goblinsDao.delete(e));
    }

    public void deleteAllWeapons () {
        weaponsDao.findAll().forEach(e-> weaponsDao.delete(e));
    }

//    public void findDefenselessGoblins() {
//        goblinsDao.findDefenselessGoblins().forEach(System.out::println);
//    }

    public void findLonelyWeapons() {
        weaponsDao.findLonelyWeapons().forEach(System.out::println);
    }

    public void findEmptyCaves() {
        cavesDao.findEmptyCaves().forEach(System.out::println);
    }

    public void findGoblinsHeigherThanGivenValue(int value) {
        goblinsDao.findGoblinsHeigherThanGivenValue(value).forEach(System.out::println);
    }
}


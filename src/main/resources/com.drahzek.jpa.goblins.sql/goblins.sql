CREATE USER 'goblins' IDENTIFIED BY 'dzban1234';

CREATE DATABASE goblins CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON goblins.* TO 'goblins';

USE goblins;

CREATE TABLE goblins (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    height INT,
    weapon INT,
    cave INT,
    PRIMARY KEY (id)
);

CREATE TABLE weapons (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    damage INT,
    damageType VARCHAR(256),
    PRIMARY KEY (id)
);

CREATE TABLE caves (
    id INT NOT NULL AUTO_INCREMENT,
    area DOUBLE,
    PRIMARY KEY (id)
);

